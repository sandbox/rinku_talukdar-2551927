(function($) {
  $(document).ready(function() {
    //console.log(Drupal.settings.social_options)
    // ["Label" , "website link" , "bar color" , "bar image"]
    var social  = Object.keys(Drupal.settings.social_options).map(function(k) { return Drupal.settings.social_options[k] });

    //console.log(social.length);
    var html = '<div id="socialside"></div>';
    $( "body" ).append(html);

    ////////////////////////////////////////////////
    //// DO NOT EDIT ANYTHING BELOW THIS LINE! /////
    ////////////////////////////////////////////////

    $("#socialside").append('<ul class="social-links"></ul>');

    /// generating bars
    for(var i=0;i<social.length;i++){
      $(".social-links").append("<li>" + '<ul class="scli" style="background-color:' + social[i][2] + '">' +
        '<li class="' + social[i][0] + '">' + social[i][0] + '</li></ul></li>');
      }

    /// bar click event
    $(".scli").click(function(){
      var link = $(this).text();
      for(var i=0;i<social.length;i++){
        if(social[i][0] == link){
          window.open(social[i][1]);
        }
      }
    });

    /// mouse hover event
    $(".scli").mouseenter(function() {
      $(this).stop(true);
      $(this).clearQueue();
        $(this).animate({
          left : "140px"
        }, 300);

    });

    /// mouse out event
    $(".scli").mouseleave(function(){
      $(this).animate({
        left:"0"
      },700,'easeOutBounce');
    });

  });
}(jQuery));
